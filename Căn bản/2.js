const rw = require('console-read-write');

async function hinhTron(){
    rw.write('Bán kính đường tròn: ');
    var bk = '';
    bk = parseFloat(await rw.read());
    
    while(isNaN(bk)){
        rw.write('Bán kính phải là một chữ số!!');
        bk = parseFloat(await rw.read());
        
    }
    
    rw.write('Chu vi đường tròn = '+(2*bk*Math.PI));
    
    rw.write('Diện tích hình tròn = '+(bk*bk*Math.PI));
}

hinhTron();

