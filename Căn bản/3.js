const rw = require('console-read-write');
var a,b,c,ab,bc,ca;
async function tamGiac(){
    await input();
    while(true){
        if(isNaN(a) || isNaN(b) || isNaN(c)){
            rw.write('Cạnh tam giác phải là một chữ số!!');
            await input();
        }
        else if(ab<=c || ca<=b || bc<=a){
            rw.write('Không phải 3 cạnh tam giác! Nhập lại.');
            await input();
        }else break;
    }
    var chuvi = a + b + c;
    var p = chuvi/2;
    rw.write('Chu vi = '+chuvi);
    rw.write('Diện tích = '+Math.sqrt(p*(p-a)*(p-b)*(p-c)));
}

async function input(){
    rw.write('a = ');
    a = parseFloat(await rw.read());
    rw.write('b = ');
    b = parseFloat(await rw.read());
    rw.write('c = ');
    c = parseFloat(await rw.read());
    
    ab = a + b;
    bc = b + c;
    ca = c + a;
    
}

tamGiac();

