const rw = require('console-read-write');

var a,b,c,d;

async function input(){
    rw.write('a = ');
    a = parseFloat(await rw.read());
    rw.write('b = ');
    b = parseFloat(await rw.read());
    rw.write('c = ');
    c = parseFloat(await rw.read());
    rw.write('d = ');
    d = parseFloat(await rw.read());
}

async function minmax(){
    await input();
    while(true){
        if(isNaN(a) || isNaN(b) || isNaN(c) || isNaN(d)){
            rw.write('Phải là số!!');
            await input();
        }else break;
    }
    var list = [];
    list.push(a);
    list.push(b);
    list.push(c);
    list.push(d);
    list.sort(function(a,b){return a - b;});
    rw.write('Min = '+list[0]);
    rw.write('Max = '+list[list.length - 1]);
    
}

minmax();


