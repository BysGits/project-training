const rw = require('console-read-write');

var a,b;

async function input(){
    rw.write('a = ');
    a = parseFloat(await rw.read());
    rw.write('b = ');
    b = parseFloat(await rw.read());
}

async function bacnhat(){
    await input();
    while(true){
        if(isNaN(a) || isNaN(b) ){
            rw.write('Phải là số!!');
            await input();
        }else break;
    }
    
    if(b>=0) rw.write('Giai phuong trinh: '+a+'x + '+b+' = 0');
    else rw.write('Giai phuong trinh: '+a+'x '+b+' = 0');
    
    if(a==0){
        if(b==0){
            rw.write('Phuong trinh vo so nghiem');
        }
        else if(b!=0){
            rw.write('Phuong trinh vo nghiem');
        }
    }else{
        var x = -b/a;
        rw.write('x = '+x);
    }
}

bacnhat();