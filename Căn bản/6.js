const rw = require('console-read-write');

var a,b, dau;

async function input(){
    rw.write('a = ');
    a = parseFloat(await rw.read());
    rw.write('b = ');
    b = parseFloat(await rw.read());
    rw.write('options: > , < , != ');
    dau = await rw.read();
}

async function batphuongtrinh(){
    await input();
    while(true){
        if(isNaN(a) || isNaN(b) ){
            rw.write('Phải là số!!');
            await input();
        }else if(dau != '>' && dau != '<' && dau != '!='){
            rw.write('Nhập lại dấu');
            await input();
        }else break;
    }
    
    if(b>=0) rw.write('Giai phuong trinh: '+a+'x + '+b+' '+dau+' 0');
    else rw.write('Giai phuong trinh: '+a+'x '+b+' '+dau+' 0');
    
    if(a==0){
        if(b==0){
            rw.write('Phuong trinh vo nghiem');
        }
        else if(b<0){
            if(dau == '>') rw.write('Luon dung');
            else rw.write('Phuong trinh vo nghiem');
        }else{
            if(dau == '<') rw.write('Luon dung');
            else rw.write('Phuong trinh vo nghiem');
        }
    }else if(a>0){
        var x = -b/a;
        rw.write('x '+dau+' '+x);
    }else{
        var x = -b/a;
        if(dau == '<') {
            dau = '>';
            rw.write('x '+dau+' '+x);
        }
        else if(dau == '>'){
            dau = '<';
            rw.write('x '+dau+' '+x);
        }else rw.write('x '+dau+' '+x);
    }
}

batphuongtrinh();


