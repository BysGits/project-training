const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function findMin(){
    var input = 0, count = 1;
    var listInt = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        if(input == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(input);
    }
    rw.write(listInt.join(", "));
    listInt.sort(function(a,b){return a - b;});
    rw.write('Min = '+listInt[0]);
}

findMin();


