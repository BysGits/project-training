const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function average(){
    var input = 0, count = 0, sum = 0;
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count < 99){
        rw.write(count+' lan');
        input = await rw.read();
        if(input == -1){
            rw.write('Average = '+ sum/count);
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        input = parseInt(input);
        sum += input;
        count++;
    }
}

average();