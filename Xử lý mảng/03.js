const rw = require('console-read-write');

async function findSame(){
    var input = 0, count = 0;
    var list = new Array();
    rw.write('Nhap so (co 99 lan de nhap)');
    rw.write('Nhap enter de stop input');
    while(count < 99){
        rw.write(count+' lan');
        input = await rw.read();
        if(input==''){
            break;
        }
        else if(isNaN(input)){
            rw.write('Nhap so di ma .');
            continue;
        }
        input = parseFloat(input);
  
        list.push(input);
        count++;
    }
    console.log(list.toString());
    rw.write('Nhap 1 so x bat ki: ');
    do{
        input = await rw.read();
        if(isNaN(input)){
            rw.write('Nhap so di ma .');
            continue;
        }
        else{
            count = 0;
            input = parseFloat(input);
            for (var x = 0;x < list.length; x++){
                if(list[x] == input){
                    count++;
                }
            }
            rw.write('So phan tu co gia tri x: '+count+' phan tu');
            break;
        }
    }while(1);
}

findSame();