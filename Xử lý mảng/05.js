const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function sortArray(){
    var input = 0, count = 1, inp;
    var listInt = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        inp = parseInt(input);
        if(inp == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(inp);
    }
    
    listInt.sort(function(a,b){return a - b;});
    rw.write(listInt.join(", "));
}

sortArray();