const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function removeElement(){
    var input = 0, count = 1, inp;
    var listInt = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        inp = parseInt(input);
        if(inp == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(inp);
    }
    rw.write('Array ban dau: '+listInt.join(', '));
    rw.write('Nhap gia tri x de xoa phan tu gia tri x: ');
    do{
        input = await rw.read();
        if(!isNumeric(input)){
            rw.write('Nhap so tu nhien x di ma .');
            continue;
        }
        else{
            count = 0;
            input = parseInt(input);
            for(var i = 0; i<listInt.length; i++){
                if(listInt[i] == input){
                    listInt.splice(i,1);
                    i--;
                    count++;
                }
            }
            rw.write('Array sau khi xoa phan tu gia tri x: '+listInt.join(', '));
            rw.write('So phan tu bi xoa: '+count);
            break;
        }
    }while(1);
}

removeElement();


