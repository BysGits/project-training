const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function insertElement(){
    var input = 0, count = 1, inp,k,x = 0;
    var listInt = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        inp = parseInt(input);
        if(inp == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(inp);
    }
    rw.write('Array ban dau: '+listInt.join(', '));
    
    do{
        rw.write('Nhap gia tri k: ');
        k = await rw.read();
        
        if(!isNumeric(k)){
            rw.write('Nhap so tu nhien k di ma .');
            continue;
        }
        else{
            if(x == 0){
                do{
                    rw.write('Nhap so tu nhien x: ');
                    x = await rw.read();
                }while(!isNumeric(x));
            }
            x = parseInt(x);
            k = parseInt(k);
            if(k > listInt.length){
                rw.write('Vuot qua so phan tu cua mang. Nhap lai k');
                continue;
            }
            listInt.splice(k-1,0,x);
            rw.write('Array sau khi chen x vao phan tu thu k: '+ listInt.join(', '));
            
            break;
        }
    }while(1);
}

insertElement();