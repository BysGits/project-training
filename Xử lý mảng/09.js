const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

function sortedIndex(array, value) {
    var low = 0,
        high = array.length;

    while (low < high) {
        var mid = Math.floor((low + high)/2);
        if (array[mid] < value) low = mid + 1;
        else high = mid;
    }
    return low;
}

async function insertSortArray(){
    var input = 0, count = 1, inp;
    var listInt = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        inp = parseInt(input);
        if(inp == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(inp);
    }
    
    listInt.sort(function(a,b){return a - b;});
    rw.write(listInt.join(", "));
    rw.write('Nhap gia tri x: ');
    do{
        input = await rw.read();
        if(!isNumeric(input)){
            rw.write('Nhap so tu nhien x di ma .');
            continue;
        }
        else{
            input = parseInt(input);
            listInt.splice(sortedIndex(listInt,input),0,input);
            rw.write(listInt.join(', '));
            break;
        }
    }while(1);
}

insertSortArray();