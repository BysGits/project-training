const rw = require('console-read-write');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

async function subArray(){
    var input = 0, count = 1, inp;
    var listInt = new Array();
    var subList = new Array();
    rw.write('Nhap so tu nhien (co 99 lan de nhap)');
    rw.write('Nhap -1 de dung');
    while(count <= 99){
        rw.write(count + ' lan');
        input = await rw.read();
        inp = parseInt(input);
        if(inp == -1){
            break;
        }
        else if(!isNumeric(input)){
            rw.write('Nhap so tu nhien di ma .');
            continue;
        }
        
        count++;
        listInt.push(inp);
    }
    
    for(var i = 1; i<listInt.length; i++){
        subList.push(listInt[i-1]);
        if(listInt[i]<listInt[i-1]){
            rw.write(subList.join(', '));
            subList.splice(0,subList.length);
        }
    }
    subList.push(listInt[listInt.length-1]);
    rw.write(subList.join(', '));
}

subArray();


